from ublox_lara_r2 import *

u = Ublox_lara_r2()
u.initialize()
u.reset_power()

# Close debug massage 
u.debug = False 
#u.debug = True 

def exec_cmd(cmd, desc, response = None, timeout = 1):
  u.send(cmd + "\r\n")
  timer = timeout
  cmd_done = False
  print "(%s) %s" % (cmd, desc), 
  while timer >= 0:
    time.sleep(0.5)
    if u.response.find(response) >= 0:
      cmd_done = True
    elif u.response.find('+CME ERROR:') >= 0:
      cmd_done = True
    elif None == response:
      cmd_done = True
    
    if cmd_done:
      break

    if u.debug:
      print timer,
      sys.stdout.flush()
    else:
      print '.',
      sys.stdout.flush()

    timer = timer - 1
    time.sleep(0.5)

  if cmd_done:
    print list(filter(None, (u.response.split('\r\n'))))[1:]
      

exec_cmd("AT+CMEE=2", "Set Error:", "OK\r\n")
exec_cmd("AT+CGMM", "Module Name:", "OK\r\n")
exec_cmd("AT+CPIN?", "SIM State:", "OK\r\n")
exec_cmd("AT+CGMI", "Manufacturer Name:", "OK\r\n")
exec_cmd("AT+CGSN", "Serial Number:", "OK\r\n")
exec_cmd("AT+CCID", "SIM Card Identification:", "OK\r\n")
exec_cmd("AT+CIMI", "SIM IMSI:", "OK\r\n")
exec_cmd("AT+CNUM", "SIM MSISDN:", "OK\r\n")
exec_cmd("AT+CSQ",  "Signal:", "OK\r\n")
exec_cmd("AT+CESQ",  "Extended Signal:", "OK\r\n")
exec_cmd("AT+COPS?",  "Operator Selected:", "OK\r\n")
exec_cmd("AT+COPS=?",  "Available Operators:", "OK\r\n", 60)

exec_cmd("AT+CGDCONT?", "Check APN:", "OK\r\n")
exec_cmd("AT+CREG?", "Check Registration:", "OK\r\n")
print "Can ignore error in checking Roaming as LARA R211 does not support command"
exec_cmd("AT+UDCONF=20", "Check Roaming:", "OK\r\n")
exec_cmd("AT+CGED=3", "Check Cell Environment:", "OK\r\n", 60)

# reset the MVO
exec_cmd("AT+COPS=2", "Deregister MNO:", "OK\r\n", 60)
exec_cmd("AT+UMNOCONF=1", "Auto Detect MNO:", "OK\r\n", 60)
exec_cmd("AT+COPS=0", "Auto Select MNO:", "OK\r\n", 60)

exec_cmd("AT+UPSD=0", "Read PSD 0:", "OK\r\n")
exec_cmd("AT+UPSD=0,1,\"super\"", "Attempt to set APN:", "OK\r\n")
exec_cmd("AT+UPSD=0", "Read PSD 0:", "OK\r\n")
exec_cmd("AT+UPSDA=0,3", "Set PSD 0 to activate", "OK\r\n", 90)
exec_cmd("AT+CGDCONT?", "Check APN:", "OK\r\n")

u.debug = True 
exec_cmd("AT+UPING=\"www.google.com\"", "Ping Google", "OK\r\n")
time.sleep(10)
u.debug = False
