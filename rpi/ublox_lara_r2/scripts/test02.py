from ublox_lara_r2 import *

u = Ublox_lara_r2()
u.initialize()
u.reset_power()

# Close debug massage 
u.debug = False 

# show module name
if u.sendAT("AT+CGMM\r\n", "OK\r\n"):
    print "\r\nmodule name: ", u.response.split('\r\n')[1]

# get SIM card state
if u.sendAT("AT+CPIN?\r\n", "OK\r\n"):
    print "\r\nSIM state: ", u.response.split('\r\n')[1]

#if u.sendAT("AT+UPSD=0,1,\"super\"\r\n", "OK\r\n"):
#    print "\r\nRegistering to APN super", u.response.split('\r\n')[1]

if u.sendAT("AT+UPSD=0\r\n", "OK\r\n"):
    print "\r\nRead Profile see debug"

if u.sendAT("AT+COPS?\r\n", "OK\r\n"):
    print "\r\nCarrier Connection", u.response.split('\r\n')[1]

#if u.sendAT("AT+UPSDA=0,3\r\n", "OK\r\n"):
#    print "\r\nActivate PDP Context", u.response.split('\r\n')[1]

if u.sendAT("AT+UPING=\"www.google.com\"\r\n", "OK\r\n"):
    print "\r\nPinging Google.com", u.response.split('\r\n')[1]

# check rssi
rssi = u.getRSSI()
print "RSSI: ", rssi
