# RasberryPi + Ublox Lara R211 (EU) 

## Setup
* `/boot/config.txt` configuration
```
[all]
#dtoverlay=vc4-fkms-v3d
dtoverlay=pi3-disable-bt
enable_uart=1
```
* disable UART0
```
sudo systemctl disable hciuart
```
* remove `console=serial0,115200` from `/boot/cmdline.txt`
```
sudo vi /boot/cmdline.txt
```
* reboot 
```
sudo reboot
```

# I2C
If you want to use I2C Grove components, turn on `dtparam=i2c_arm=on` in `/boot/config.txt`
Reference [Various options for RPi config.txt](https://www.raspberrypi.org/forums/viewtopic.php?f=28&t=97314)

[List of Seeed Grove Module with I2C Interface](https://wiki.seeedstudio.com/I2C_And_I2C_Address_of_Seeed_Product/)
